<?php

namespace Liquidlib\LaravelCaptcha;

use Illuminate\Support\ServiceProvider;


class CaptchaServiceProvider extends ServiceProvider
{

    /**
     * 在容器中注册绑定。
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('captcha-api', function () {
            return new CaptchaApi();
        });
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('captcha.php'),
        ]);
    }


}
